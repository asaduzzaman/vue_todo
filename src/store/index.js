import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    idForTodo: 1,
    todos: []
  },
  mutations: {
    addTodo(state, payload) {
      console.log("i am in addTodo");
      state.todos.push({
        id: state.idForTodo,
        title: payload,
        completed: false,
        editing: false
      });
      state.idForTodo++;
    },
    updateTodo(state, payload) {
      console.log("i am in updateTodo");
      let index = state.todos.findIndex(x => x.id === payload.id);
      state.todos.splice(index, 1, {
        id: payload.id,
        title: payload.title,
        completed: payload.completed,
        editing: false
      });
    },
    removeTodo(state, payload) {
      console.log("i am in removeTodo");
      state.todos.splice(payload, 1);
    },
    completeTodo(state, payload) {
      console.log("i am in completeTodo");
      state.todos.find(x => {
        if (x.id === payload.todo.id) {
          x.completed = payload.event;
        }
      });
    },
    checkAllTodos(state, payload) {
      console.log("i am in checkAllTodos");
      state.todos.forEach(x => (x.completed = payload));
    },
    clearCompleted(state) {
      console.log("i am in clearCompleted");
      let todos = state.todos.filter(x => !x.completed);
      state.todos = todos;
    }
  },
  actions: {},
  modules: {}
});
